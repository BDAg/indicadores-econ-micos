# Indicadores Econômicos
**O Projeto :**

Nosso projeto consiste em coletar as cotações, Data,Abertura,Media,Maxima,Minima e Variacao do Dolar para que se possa relacionar com demais informacoes e salvando em um arquivo CSV,
 minerando e explorando um site https://br.investing.com/. O projeto foi trabalhado também e postado no GitHub, uma plataforma semelhante ao GitLab executando a mesma funções semelhantes.
 
Link do GitHub:

https://github.com/pan963/Crawler

**Documentação:**


[Cronograma_Indicadores_Econômicos.odp](uploads/7f5f6081195cc6aeec7c6c08a170fb83/Cronograma_Indicadores_Econômicos.odp)

[Matriz_de_habilidade.pdf](uploads/a06dbd36df4a7928b868b49fe4ab485e/Matriz_de_habilidade.pdf)

[MvpProgetoIntegrador.pdf](uploads/f0a7abbdd9e7deccad729a7cf1782484/MvpProgetoIntegrador.pdf)

[Roteiro_Bot.pdf](uploads/687b952f5bd6146768943bff289eab24/Roteiro_Bot.pdf)

[Sprint.pdf](uploads/b8fe68fc3d2b642fc0e8cc54b11a15c9/Sprint.pdf)

**Equipe:**

- [Carlos Henrique Carvalho Soares](https://gitlab.com/BDAg/indicadores-econ-micos/-/wikis/Carlos-Soares) 

- [João Luiz Pan Toratti]()

- [Robson da Silva Campello Junior]()


**Como utilizar o Crawler**

Baixe o Script Bot uma moeda ou BotAutomatizado e execute no diretorio ou Vscode ou Idle.(para o BotdeUmaMoeda e necessario colocar os parametro da moeda):

[BotAutomatizadopara_todas_as_moedas.py](uploads/0d0510bc4a110be9202586aa38439b24/BotAutomatizadopara_todas_as_moedas.py)

[BotManualUmamoeda.py](uploads/84869e836f3f7e5b70e2129919f3aa7c/BotManualUmamoeda.py)


**Python**

O Crawler foi estruturado em Crawler, portanto e necessario ele para executa-lo.

sudo apt-get install python3-pip

**Bibliotecas**

Foram usadas as seguintes Bibliotecas:

requests

csv

time

multiprocessing

logging

datetime

lxml html

**bs4:**

pip3 install bs4

**requests:**

pip3 install requests

**lxml:**

pip3 install lxml

**pymongo:**

pip3 install pymongo

pip3 install dnspython

**Para verificar se as bibliotecas foram instaladas basta utilizar o comando:**

pip3 list

